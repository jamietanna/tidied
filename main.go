package main

import (
	"bytes"
	"flag"
	"fmt"
	"os"
	"os/exec"
)

func main() {
	var verbose bool
	flag.BoolVar(&verbose, "verbose", false, "whether to log the full diff of changes present in `go.mod` and `go.sum`")
	flag.Parse()

	data, err := diff()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to run `git diff`: %v\n", err)
	}
	if len(data) != 0 {
		fmt.Fprintf(os.Stderr, "Uncommitted changes are already present, please commit and try again:\n%s\n", string(data))
		os.Exit(2)
	}

	tidyCmd := exec.Command("go", "mod", "tidy")
	err = tidyCmd.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to execute `go mod tidy`: %v\n", err)
		os.Exit(1)
	}

	statusCmd := exec.Command("git", "status", "--porcelain", "go.mod", "go.sum")
	var out bytes.Buffer
	statusCmd.Stdout = &out

	err = statusCmd.Run()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to run `git status`: %v\n", err)
		os.Exit(1)
	}

	if out.Len() != 0 {
		if verbose {
			fmt.Printf("Detected untracked changes after running `go mod tidy`:\n%s\n", out.Bytes())
			bytes, err := diff()
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to run `git diff`: %v\n", err)
			} else {
				fmt.Printf("%s\n", bytes)
			}
		}

		os.Exit(1)
	}
}

func diff() ([]byte, error) {
	diffCmd := exec.Command("git", "diff", "--color", "go.mod", "go.sum")
	var out bytes.Buffer
	diffCmd.Stdout = &out

	err := diffCmd.Run()
	if err != nil {
		return []byte{}, err
	}
	return out.Bytes(), nil
}

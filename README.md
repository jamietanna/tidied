# Is your `go.mod` and `go.sum` tidied?

Tidied is a command-line interface to ensure that your Go projects have run `go mod tidy`.

This is a stop-gap solution until [support is native in the Go toolchain](https://github.com/golang/go/issues/27005).

This relies on the `go` and `git` toolchains to be present.

## Usage

```sh
# although I usually recommend pinning, you could also just grab latest. I promise I'll try very hard to avoid breaking changes!
$ go install gitlab.com/jamietanna/tidied@latest
# non-zero exit code means there are changes
$ tidied && echo $?
1
# alternatively, make it verbose
$ tidied -verbose && echo $?
Detected untracked changes after running `go mod tidy`:
...
1
```
